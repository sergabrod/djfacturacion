from django.db import models
from django.contrib.auth.models import User

class Modelo(models.Model):
    estado = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_modifica = models.DateTimeField(auto_now=True)
    user_creacion = models.ForeignKey(
        User, 
        on_delete=models.CASCADE,
        # colocamos %(class)s delante de cada relate_name para evitar colisiones
        # al heredar los hijos
        related_name='%(class)s_User_Create',
        verbose_name = 'Creado por',
    )

    user_modifica = models.ForeignKey(
        User, 
        on_delete=models.CASCADE, 
        related_name='%(class)s_User_Update',
        verbose_name = 'Modificado por',
        blank = True, 
        null = True,
    )

    # con astract=True, le decimos que no tenga en cuenta el model para las migraciones
    class Meta:
        abstract = True