from django import forms
from .models import Categoria, Subcategoria, Marca, UnidadMedida

class CategoriaForm(forms.ModelForm):
    class Meta:
        model = Categoria
        fields = ['descripcion', 'estado']
        labels = {'descripcion':'Descripción', 'estado':'Estado'}
        widget = {'descripcion': forms.TextInput}

    # sobreescribo el método init para asignar la class a todos el elementos del form
    # también puede hacerse con cada elemento 
    # widget = {'descripcion': forms.TextInput(attrs={'class':'form-control'})}
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control',
            })


class SubcategoriaForm(forms.ModelForm):
    # modificamos el queryset para que sólo muestre los activos
    categoria = forms.ModelChoiceField(
        queryset = Categoria.objects.filter(estado=True)
        .order_by('descripcion')
    )
    class Meta:
        model = Subcategoria
        fields = ['categoria', 'descripcion', 'estado']
        labels = {'descripcion':'Sub Categoría', 'estado':'Estado'}
        widget = {'descripcion': forms.TextInput}

    # sobreescribo el método init para asignar la class a todos el elementos del form
    # también puede hacerse con cada elemento 
    # widget = {'descripcion': forms.TextInput(attrs={'class':'form-control'})}
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control',
            })
        
        self.fields['categoria'].empty_label = 'Seleecione categoría'


class MarcaForm(forms.ModelForm):
    class Meta:
        model = Marca
        fields = ['descripcion', 'estado']
        labels = {'descripcion':'Descripción', 'estado':'Estado'}
        widget = {'descripcion': forms.TextInput}

    # sobreescribo el método init para asignar la class a todos el elementos del form
    # también puede hacerse con cada elemento 
    # widget = {'descripcion': forms.TextInput(attrs={'class':'form-control'})}
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control',
            })

class UnidadMedidaForm(forms.ModelForm):
    class Meta:
        model = UnidadMedida
        fields = ['descripcion', 'estado']
        labels = {'descripcion':'Descripción', 'estado':'Estado'}
        widget = {'descripcion': forms.TextInput}

    # sobreescribo el método init para asignar la class a todos el elementos del form
    # también puede hacerse con cada elemento 
    # widget = {'descripcion': forms.TextInput(attrs={'class':'form-control'})}
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class':'form-control',
            })