from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from .models import Categoria, Subcategoria, Marca, UnidadMedida
from .forms import CategoriaForm, SubcategoriaForm, MarcaForm, UnidadMedidaForm

class CategoriaView(LoginRequiredMixin, ListView):
    model = Categoria
    # no hace falta lo que sigue abajo ya que uso los 
    # template_name y context_object_name que toma por defecto: 
    # className_list.html y className_list respectivamente
    # template_name = "inv/categoria_list.html"
    # context_object_name = "obj"
    login_url = 'bases:login'


class CategoriaNew(LoginRequiredMixin, CreateView):
    model = Categoria
    # template_name = 'inv/categoria_form.html'
    form_class = CategoriaForm
    success_url = reverse_lazy('inv:categoria_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_creacion = self.request.user
        return super().form_valid(form)


class CategoriaEdit(LoginRequiredMixin, UpdateView):
    model = Categoria
    # template_name = 'inv/categoria_form.html'
    form_class = CategoriaForm
    success_url = reverse_lazy('inv:categoria_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_modifica = self.request.user
        return super().form_valid(form)


class CategoriaDel(LoginRequiredMixin, DeleteView):
    model = Categoria
    template_name = 'inv/catalogo_del.html'
    # como el template de borrado es genérico para varias objetos del catálogo 
    # llamo obj_del al objeto
    context_object_name = 'obj_del'
    success_url = reverse_lazy('inv:categoria_list')
    login_url = 'bases:login'


class SubcategoriaView(LoginRequiredMixin, ListView):
    model = Subcategoria
    login_url = 'bases:login'


class SubcategoriaNew(LoginRequiredMixin, CreateView):
    model = Subcategoria
    form_class = SubcategoriaForm
    success_url = reverse_lazy('inv:subcategoria_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_creacion = self.request.user
        return super().form_valid(form)


class SubcategoriaEdit(LoginRequiredMixin, UpdateView):
    model = Subcategoria
    form_class = SubcategoriaForm
    success_url = reverse_lazy('inv:subcategoria_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_modifica = self.request.user
        return super().form_valid(form)

class SubcategoriaDel(LoginRequiredMixin, DeleteView):
    model = Subcategoria
    template_name = 'inv/catalogo_del.html'
    context_object_name = 'obj_del'
    success_url = reverse_lazy('inv:subcategoria_list')
    login_url = 'bases:login'


class MarcaView(LoginRequiredMixin, ListView):
    model = Marca
    # no hace falta lo que sigue abajo ya que uso los 
    # template_name y context_object_name que toma por defecto: 
    # className_list.html y className_list respectivamente
    # template_name = "inv/categoria_list.html"
    # context_object_name = "obj"
    login_url = 'bases:login'

class MarcaNew(LoginRequiredMixin, CreateView):
    model = Marca
    # template_name = 'inv/categoria_form.html'
    form_class = MarcaForm
    success_url = reverse_lazy('inv:marca_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_creacion = self.request.user
        return super().form_valid(form)

class MarcaEdit(LoginRequiredMixin, UpdateView):
    model = Marca
    # template_name = 'inv/categoria_form.html'
    form_class = MarcaForm
    success_url = reverse_lazy('inv:marca_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_modifica = self.request.user
        return super().form_valid(form)


# Para inactivar usaremos vistas basadas en funciones
def marca_inactivar(request, id):
    marca = Marca.objects.filter(pk=id).first()
    contexto =  {}
    template = "inv/catalogo_del.html"

    if not marca:
        return redirect("inv:marca_list")
    
    if request.method == 'GET':
        contexto = {'obj_del':marca}
    
    if request.method == 'POST':
        marca.estado = False
        marca.save()
        return redirect("inv:marca_list")
    
    return render(request, template, contexto)


class UnidadMedidaView(LoginRequiredMixin, ListView):
    model = UnidadMedida
    login_url = 'bases:login'

class UnidadMedidaNew(LoginRequiredMixin, CreateView):
    model = UnidadMedida
    form_class = UnidadMedidaForm
    success_url = reverse_lazy('inv:unidadmedida_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_creacion = self.request.user
        return super().form_valid(form)

class UnidadMedidaEdit(LoginRequiredMixin, UpdateView):
    model = UnidadMedida
    # template_name = 'inv/categoria_form.html'
    form_class = UnidadMedidaForm
    success_url = reverse_lazy('inv:unidadmedida_list')
    login_url = 'bases:login'

    # modificamos el form en el form_valid para guardar el usuario
    def form_valid(self, form):
        form.instance.user_modifica = self.request.user
        return super().form_valid(form)

def unidadmedida_inactivar(request, id):
    unida_medida = UnidadMedida.objects.filter(pk=id).first()
    contexto =  {}
    template = "inv/catalogo_del.html"

    if not unida_medida:
        return redirect("inv:unidadmedida_list")
    
    if request.method == 'GET':
        contexto = {'obj_del':unida_medida}
    
    if request.method == 'POST':
        unida_medida.estado = False
        unida_medida.save()
        return redirect("inv:unidadmedida_list")
    
    return render(request, template, contexto)

