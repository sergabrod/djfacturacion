from django.db import models
from bases.models import Modelo

class Categoria(Modelo):
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción',
        unique=True, 
    )

    def __str__(self):
        return '{}'.format(self.descripcion)

    # sobreescribimos el mètodo save para guardar la descripción en mayúsculas
    def save(self):
        self.descripcion = self.descripcion.upper()
        super(Categoria, self).save()

    class Meta:
        verbose_name_plural = 'Categorías'


class Subcategoria(Modelo):
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción',
        unique=True, 
    )

    def __str__(self):
        return '{}:{}'.format(self.categoria.descripcion, self.descripcion)

    # sobreescribimos el mètodo save para guardar la descripción en mayúsculas
    def save(self):
        self.descripcion = self.descripcion.upper()
        super(Subcategoria, self).save()
    
    class Meta:
        verbose_name_plural = 'Subcategorías'
        # Evitamos que se repitan las subcategorias dentro de cada categoria
        # no podemos usar unique porque pueden repetirse dentro de diferentes
        # Categorias, por ej, todas las categorias pueden tener un "todos"
        unique_together = ('categoria', 'descripcion')


class Marca(Modelo):
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción',
        unique=True, 
    )

    def __str__(self):
        return '{}'.format(self.descripcion)

    # sobreescribimos el mètodo save para guardar la descripción en mayúsculas
    def save(self):
        self.descripcion = self.descripcion.upper()
        super(Marca, self).save()

    class Meta:
        verbose_name_plural = 'Marcas'


class UnidadMedida(Modelo):
    descripcion = models.CharField(
        max_length=100,
        help_text='Descripción',
        unique=True, 
    )

    def __str__(self):
        return '{}'.format(self.descripcion)

    # sobreescribimos el mètodo save para guardar la descripción en mayúsculas
    def save(self):
        self.descripcion = self.descripcion.upper()
        super(UnidadMedida, self).save()

    class Meta:
        verbose_name_plural = 'Unidades de Medida'
